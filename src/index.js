import ReactDOM from 'react-dom/client';
import {Layout} from "./components/layout";
import {BrowserRouter} from "react-router-dom";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import {Router} from "./core/router";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <BrowserRouter>
        <Layout>
        </Layout>
        <Router/>
        <ToastContainer/>
    </BrowserRouter>
)
;