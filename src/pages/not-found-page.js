import {Link} from "react-router-dom";
import Button from "@mui/material/Button";
import "./not-found.css"

export const NotFoundPage = () => (
    <div className="page-not-found-container">
        <h2>Oops, something went wrong.</h2>
        <Button variant="contained">
            <Link to="/" style={{ textDecoration: 'none', color: '#fff' }}>Go Back</Link>
        </Button>
    </div>
);