import * as React from 'react';
import "./home.css"
import {TextField} from "@mui/material";
import Button from "@mui/material/Button";
import SearchIcon from '@mui/icons-material/Search';
import {useRef, useState} from "react";
import {toast} from "react-toastify";
import {FactComponent} from "../components/fact-component";
import {QueryClient, QueryClientProvider} from "react-query";
import {SimpleFactContainer} from "../components/simple-fact-container";

const queryClient = new QueryClient()
export const Home = () => {
    const searchValue = useRef('');
    const [facts, setFacts] = useState([]);
    const [search, setSearch] = useState(false);
    const [searchData, setSearchData] = useState('');
    const saved = localStorage.getItem("fact");
    if (facts.length === 0) {
        if (saved) {
            let data = JSON.parse(saved);
            if (data?.length > 10) {
                data = data.slice(0, 10);
            }
            setFacts(data);
        } else {
            fetch("https://api.chucknorris.io/jokes/random")
                .then(res => res.json())
                .then(
                    (result) => {
                        setFacts([result]);
                    },
                    (error) => {
                        toast.error('Error retrieving Chuck Norris facts')
                    }
                )
        }
    }

    const getFact = () => {
        const value = searchValue?.current?.value
        if (value?.length >= 4) {
            setSearch(true);
            setSearchData(searchValue?.current?.value);
        } else {
            toast.info('Fact param must contain at least 4 character');
        }
    }

    return (
        <div className="home-container">
            <div className="search-container">
                <TextField id="outlined-basic" label="Fact" variant="outlined" inputRef={searchValue}
                           style={{width: '300px', borderRight: 'none'}}/>
                <Button variant="contained" className='search-icon' onClick={getFact}>
                    <SearchIcon/>
                </Button>
            </div>
            {
                search &&
                <div className="all-facts-container">
                    <QueryClientProvider client={queryClient}>
                        <SimpleFactContainer searchValue={searchData}/>
                    </QueryClientProvider>
                </div>
            }

            <h4 style={{fontSize: 30, textAlign: 'left'}}>Search history:</h4>
            <div className="fact-container">
                {facts.map((fact, index) => (
                    <FactComponent key={index} fact={{...fact}}/>
                ))}
            </div>
        </div>
    );
}