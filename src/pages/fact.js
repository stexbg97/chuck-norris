import {useLocation, useNavigate} from "react-router-dom";
import './fact.css';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import {FactComponent} from "../components/fact-component";

export const Fact = () => {
    const {state} = useLocation();
    const navigate = useNavigate();
    const fact = state?.fact?.fact;
    const goBack = () => {
        navigate('/');
    }

    if (fact) {
        const saved = localStorage.getItem("fact");
        if (saved) {
            const data = JSON.parse(saved);
            data.push(fact);
            localStorage.setItem("fact", JSON.stringify(data));
        } else {
            localStorage.setItem("fact", JSON.stringify([fact]));
        }
    } else {
        goBack();
    }


    return (
        <div className='single-fact-container'>
            <div className="single-fact-header">
                <ArrowBackIcon style={{cursor: 'pointer', fontSize: 30}} onClick={goBack}/>
                <h4 style={{marginLeft: 30, fontSize: 25}}>Single Fact</h4>
            </div>
            <div className="single-fact">
                <FactComponent fact={fact}/>
            </div>
        </div>
    )
}