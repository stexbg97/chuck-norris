import {Route, Routes} from "react-router-dom";
import {Home} from "../pages/home";
import {QueryClient, QueryClientProvider} from "react-query";
import {Fact} from "../pages/fact";
import {NotFoundPage} from "../pages/not-found-page";

const queryClient = new QueryClient()
export const Router = () => (
    <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/details/:id"
               element={<QueryClientProvider client={queryClient}><Fact/> </QueryClientProvider>}/>
        <Route path="*" element={<NotFoundPage/>}/>
    </Routes>
)