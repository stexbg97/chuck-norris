import {useQuery} from "react-query";
import {toast} from "react-toastify";
import {SimpleFact} from "./simple-fact";
import {NoResult} from "./no-result";

export const SimpleFactContainer = (searchValue) => {
    let currentSearchValue = '';
    let facts = [];
    const {isLoading, error, data, refetch} = useQuery('fact', () =>
        fetch(`https://api.chucknorris.io/jokes/search?query=${searchValue?.searchValue}`).then(res =>
            res.json()
        ), {enabled: false}
    )

    if (currentSearchValue !== searchValue) {
        currentSearchValue = searchValue;
        refetch();
    }

    if (data?.result) {
        if (data.result.length === 0) {
            return (<NoResult/>);
        }
        facts = data.result.slice(0, 10);
    } else {
        return (<NoResult/>);
    }

    if (isLoading) return 'Loading...'

    if (error) toast.error('Error retrieving data')

    return (
        <div>
            {facts?.map((fact, index) => (
                <SimpleFact key={index} fact={fact}/>
            ))}
        </div>
    )

}