import './no-result.css';

export const NoResult = () => (
    <div className="no-result-container">
        <span>No result</span>
    </div>
)