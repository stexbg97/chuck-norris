import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

export const FactComponent = ({fact}) => {
    if (fact) {
        return (
            <Card sx={{maxWidth: 345, mb: 5}}>
                <CardMedia
                    component="img"
                    height="140"
                    image={fact.icon_url}
                    alt="Icon"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {fact.value}
                    </Typography>
                </CardContent>
            </Card>
        )
    }
}