import "./simple-fact.css"
import { useNavigate } from 'react-router-dom';

export const SimpleFact = (fact) => {
    const navigate = useNavigate();

    const goToFact = () => {
        navigate(`/details/${fact.fact.id}`, {state: {fact}});
    }
    return (
        <div className="simple-fact-container" onClick={goToFact}>
            {fact.fact.value}
        </div>
    );
}